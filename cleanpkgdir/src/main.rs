use regex::Regex;
use std::{env, io, process};
use std::fs;
use std::collections::HashMap;
use std::io::Write;
use std::path::PathBuf;

// Function that will expand "." to current directory if used as directory input parameter
fn normalize_path(dir_path: &str) -> std::io::Result<PathBuf> {
    let dir_path = PathBuf::from(dir_path);
    if dir_path == PathBuf::from(".") {
        env::current_dir()
    }
    else {
        Ok(dir_path)
    }
}

fn main() {
    // Prints extra info to help debug parsing steps
    let debug_mode = false;

    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} <directory> <number of packages to keep>(defaults to 2)", args[0]);
        process::exit(1);
    }

    let mut dir_path = args[1].clone();

    // Here is where we expand "." to current directory if provided as directory
    match normalize_path(&dir_path) {
        Ok(normalized_path) => dir_path = normalized_path.to_string_lossy().to_string(),
        Err(e) => eprintln!("Failed to normalize path: {}", e)
    }

    // Add a trailing slash to the directory if none provided
    if !dir_path.ends_with('/') {
        dir_path.push('/');
    }

    if debug_mode {
        println!("Searching directory: {}", dir_path);
    }

    // Accept u8 integer for second argument, else default to 2
    let package_retention_count = if args.len() == 3 {
        match args[2].parse::<u8>() {
            Ok(n) => n,
            Err(_) => {
                println!("Error: second argument must be u8 integer or left blank.");
                process::exit(1)
            },
        }
    } else {
        2
    };

    // This regex is trying to match one of three variations of this pattern: <app name>-<version>-<revision>
    // The differences are in the version syntax. These three file names represent the patterns:
    // lib32-obs-vkcapture-git-r256.014c1ef-1-x86_64.pkg.tar.zst
    // libquotient-git-0.8.0.r44.g65681351-1-x86_64.pkg.tar.zst
    // lmdbxx-1.0.0-1-any.pkg.tar.zst
    let re = Regex::new(r"^(.+[^0-9]*)-([\d+.]*)-(\d+)|^(.+[^0-9]*)-([rv][0-9]+\.[a-f0-9]+)-(\d+)|^(.+[^0-9]*)-([\d+.]*[rv][0-9]+\.[a-z0-9]+)-(\d+)").unwrap();

    let mut files: Vec<String> = Vec::new();

    if let Ok(entries) = fs::read_dir(&dir_path) {
        for entry in entries {
            if let Ok(entry) = entry {
                let file_name = entry.file_name().to_string_lossy().to_string();
                if let Some(_) = re.captures(&file_name) {
                    files.push(file_name);
                }
            }
        }

        // The file list is sorted in reverse so that oldest versions of apps are printed later
        files.sort_unstable_by(|a, b| b.cmp(a));

        // Keep count of same packages
        let mut counts = HashMap::new();
        // List of fullpath file names for the remove_file command
        let mut purge_list: Vec<String> = Vec::new();

        // This is where the regex comes into play to parse each file name
        // 'version' and 'revision' are only used in debugging mode at this time to validate the regex is parsing as expected
        // 'version' and 'revision' could be used to compare older releases versus newer, but it would duplicate the work of 'files.sort_unstable_by' reverse sorting completed earlier
        for file_name in &files {
            if let Some(captures) = re.captures(&file_name) {
                // Since there are three regex Matches, we must call for the correct respective Capture Group per Match
                let appname = captures.get(1).or_else(|| captures.get(4).or_else(|| captures.get(7))).unwrap().as_str();
                let version = captures.get(2).or_else(|| captures.get(5).or_else(|| captures.get(8))).unwrap().as_str();
                let revision = captures.get(3).or_else(|| captures.get(6).or_else(|| captures.get(9))).unwrap().as_str();

                // Each time an app name is seen, add a count next to it in the 'counts' hash table
                let count = counts.entry(appname).or_insert(0);
                *count += 1;

                // If any of the app names were seen more times than the threshold, add the full path of the excessive file(s) to the purge_list
                if *count > package_retention_count {
                    purge_list.push(String::from(dir_path.to_owned()+file_name));
                    if debug_mode {
                        println!("Debug: File Name: {}", file_name);
                        println!("Debug: Full Path: {}{}", dir_path, file_name);
                        println!("Debug: App Name: {}", appname);
                        println!("Debug: Version: {}", version);
                        println!("Debug: Revision: {}", revision);
                    }
                }
            }
        }

        if purge_list.is_empty() {
            println!("No file results from the retention threshold criterion of {}. Exiting.", package_retention_count);
            process::exit(0);
        }
        else {
            // As long as there were results, print them
            println!("The following files exceed the retention threshold of {}:", package_retention_count);

            for package_fullpath in &purge_list {
                println!("{}", package_fullpath);
            }

            let mut user_purge_decision = String::new();

            // Ask user whether or not to purge the resulting files. Repeat until response matches Y/N
            loop {
                println!("Remove above files? (Y/N)");
                io::stdout().flush().unwrap();
                io::stdin().read_line(&mut user_purge_decision).unwrap();
                match user_purge_decision.trim() {
                    // Unless in debug mode, delete the resultant files when response is Y
                    "Y" | "y" => {
                        for package_fullpath in &purge_list {
                            if debug_mode {
                                println!("Debug: fs::remove_file({})", &package_fullpath);
                            } else {
                                fs::remove_file(&package_fullpath).expect(&format!("Failed to remove file: {}. Do you have write permission to parent directory?", &package_fullpath));
                            }
                        }
                        break;
                    },
                    "N" | "n" => {
                        println!("Aborting....");
                        process::exit(0);
                    },
                    _ => {
                        println!("Invalid input! You must respond with Y or N.");
                        user_purge_decision.clear();
                    }
                }
            }
            println!("Done!");
        }
    } else {
        println!("Failed to read directory: {}", &dir_path);
    }
}
