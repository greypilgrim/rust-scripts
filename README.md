# rust-scripts

## Description of Project
I want to learn to write Rust scripts and share the experience. Scripts that I think are
in a usable state and can be shared publicly will be posted here.

## Descriptions of Scripts

### cleanpkgdir (Install from the [AUR](https://aur.archlinux.org/packages/cleanpkgdir))

The purpose is to parse a local Arch Linux package directory and only return results
when there are more than two (2) versions of the same package in the directory. You can optionally
override the package retention count. Then, it will list the oldest versions of those packages and prompt to purge them.

The suggested use-case is to clean up old versions of AUR packages. In **/etc/makepkg.conf**, set `PKGDEST=/user_provided_path`. ([Reference](https://wiki.archlinux.org/title/Makepkg#Package_output))
Doing so saves all of your AUR builds to one place, perhaps a network share or synchronized directory.
Practice this for a long time, and now you need to clean up old packages somehow. The cleanpkgdir program makes that quick and easy, similar to what [paccache](https://wiki.archlinux.org/title/Pacman#Cleaning_the_package_cache) does for official packages.

#### In fact, during this project, I learned you can use `paccache -c /user_provided_path -rk2` to do the same thing cleanpkgdir does. Explore the [paccache Bash code here](https://gitlab.archlinux.org/pacman/pacman-contrib/-/blob/16e65116b2cbf6eb249b960575e9f56a0e837dab/src/paccache.sh.in) to compare.

Testing:
```shell
cd cleanpkgdir/src
cargo run <package directory>
```
or
`cargo run <package directory> <# of packages to keep>`

Example directory contents:
```text
czkawka-cli-5.0.2-1-x86_64.pkg.tar.zst
czkawka-cli-5.1.0-1-x86_64.pkg.tar.zst
czkawka-cli-6.0.0-0.1-x86_64.pkg.tar.zst
czkawka-cli-6.0.0-0.2-x86_64.pkg.tar.zst
czkawka-gui-5.0.2-1-x86_64.pkg.tar.zst
czkawka-gui-5.1.0-1-x86_64.pkg.tar.zst
czkawka-gui-6.0.0-0.1-x86_64.pkg.tar.zst
czkawka-gui-6.0.0-0.2-x86_64.pkg.tar.zst
lib32-mangohud-0.6.7-1-x86_64.pkg.tar.zst
lib32-mangohud-x11-0.6.7-2-x86_64.pkg.tar.zst
lib32-obs-vkcapture-git-r231.35e147b-1-x86_64.pkg.tar.zst
lib32-obs-vkcapture-git-r256.014c1ef-1-x86_64.pkg.tar.zst
libquotient-git-0.8.0.r44.g65681351-1-x86_64.pkg.tar.zst
lmdbxx-1.0.0-1-any.pkg.tar.zst
obs-vkcapture-git-r231.35e147b-1-x86_64.pkg.tar.zst
obs-vkcapture-git-r256.014c1ef-1-x86_64.pkg.tar.zst
unityhub-3.3.0-2-x86_64.pkg.tar.zst
unityhub-3.4.1-1-x86_64.pkg.tar.zst
unityhub-3.4.2-1-x86_64.pkg.tar.zst
unityhub-3.5.0-1-x86_64.pkg.tar.zst
zoom-5.11.3-1-x86_64.pkg.tar.zst
zoom-5.12.6-1-x86_64.pkg.tar.zst
zoom-5.14.7-1-x86_64.pkg.tar.zst
zoom-5.15.2-1-x86_64.pkg.tar.zst
```

Example results:
```text
The following files exceed the retention threshold of 2:
/user_provided_path/zoom-5.12.6-1-x86_64.pkg.tar.zst
/user_provided_path/zoom-5.11.3-1-x86_64.pkg.tar.zst
/user_provided_path/unityhub-3.4.1-1-x86_64.pkg.tar.zst
/user_provided_path/unityhub-3.3.0-2-x86_64.pkg.tar.zst
/user_provided_path/czkawka-gui-5.1.0-1-x86_64.pkg.tar.zst
/user_provided_path/czkawka-gui-5.0.2-1-x86_64.pkg.tar.zst
/user_provided_path/czkawka-cli-5.1.0-1-x86_64.pkg.tar.zst
/user_provided_path/czkawka-cli-5.0.2-1-x86_64.pkg.tar.zst
Remove above files? (Y/N)
```

Example results in debug mode:
```text
File name: zoom-5.12.6-1-x86_64.pkg.tar.zst
App Name: zoom
Version: 5.12.6
Revision: 1
File name: zoom-5.11.3-1-x86_64.pkg.tar.zst
App Name: zoom
Version: 5.11.3
Revision: 1
File name: unityhub-3.4.1-1-x86_64.pkg.tar.zst
App Name: unityhub
Version: 3.4.1
Revision: 1
File name: unityhub-3.3.0-2-x86_64.pkg.tar.zst
App Name: unityhub
Version: 3.3.0
Revision: 2
File name: czkawka-gui-5.1.0-1-x86_64.pkg.tar.zst
App Name: czkawka-gui
Version: 5.1.0
Revision: 1
File name: czkawka-gui-5.0.2-1-x86_64.pkg.tar.zst
App Name: czkawka-gui
Version: 5.0.2
Revision: 1
File name: czkawka-cli-5.1.0-1-x86_64.pkg.tar.zst
App Name: czkawka-cli
Version: 5.1.0
Revision: 1
File name: czkawka-cli-5.0.2-1-x86_64.pkg.tar.zst
App Name: czkawka-cli
Version: 5.0.2
Revision: 1
```

## Contributing
If you find a bug, very inefficient or dangerous code, I welcome posting an Issue.

New features can be suggested by creating an Issue, however I may not choose to implement it.
